# D&D Homebrew

This repository tracks my homebrew classes, races, and other documents which I work on. 


## Document Format

The format of the documents follow 'gmbinder-style' markdown. Most these documents will be copied and released on GMBinder for nice formatting for official releases, but are tracked here so that I have a personal copy which is not dependent on the existence of GMBinder.
