### Death Pact Signet (Flawed)

*Ring, Common*

<img
    style="width:250px;margin-bottom:-100px;margin-top:-90px" src='https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/dc348a30-8884-4866-950b-f6818d546ba7/deypjua-1ae9856b-ac2a-4361-acc1-faedcd872e03.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2RjMzQ4YTMwLTg4ODQtNDg2Ni05NTBiLWY2ODE4ZDU0NmJhN1wvZGV5cGp1YS0xYWU5ODU2Yi1hYzJhLTQzNjEtYWNjMS1mYWVkY2Q4NzJlMDMucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.DvslHgjirKq9peh-1hi0pZ8_Hzm651VQhkL256XQyqk'
/>

<div class='centered'>

*Credit: Farwind, Based on art by ArenaNet*

</div>

___
This ring can be used to cast the *raise dead* or *revivify* spells as an action while the ring is unattuned and you have a free attunement slot. When you successfully resurrect a target this way, you are automatically attuned to the ring, and are unable to break the attunement.

If either you or the target you resurrected die, or the attunement is broken by any means except for the *wish* spell, both you and the target you resurrected die. If you or your target die in this way, you cannot be resurrected by any spell other than *true resurrection*.

If someone attuned to the ring enters an anti-magic field, their health is immediately reduced to zero, and the target they resurrected immediately falls unconscious. both individuals cannot regain consciousness under any means until the person attuned to the ring is removed from the anti-magic field.


<div class='descriptive'>

 ##### A Plague Among the Desperate

 Death Pact Signets are, unfortunately, one of the most common magical items that can be found in the world. Unsavory folk always seem able to find a supply -- both of the signets themselves and the unending mass of desperate individuals who would do or risk anything to gain their loved ones back.

</div>
