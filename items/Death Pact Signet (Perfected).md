### Death Pact Signet (Perfected)

*Ring, Rare*

___
This ring can be used to cast the *resurrect*, *raise dead*, or *revivify* spells as an action while the ring is unattuned and you have a free attunement slot. When you successfully resurrect a target this way, you are automatically attuned to the ring, and are unable to break the attunement.

If either you or the target you resurrected die within the next sixty days, or the attunement is broken by any means except for the *wish* spell, both you and the target you resurrected die. After the sixty day period is up, the attunement automatically ends. If you or the target you resurrected die in this way, you cannot be resurrected by any spell other than *true resurrection*.

If someone attuned to the ring enters an anti-magic field, both they and the person they resurrected fall unconscious. the cannot regain consciousness under any means until they are removed from the anti-magic field.

<div class='descriptive'>

 ##### Differentiating Flawed and Perfected Death Pact Signets

 Unfortunately, the most minute error in a death pact signet can cause the signet to not work as intended, and craftsmen often make hundreds of flawed signets in the pursuit of a perfected signet. One must pass an *Arcana*, *Religion*, or *Jeweler's Tools* check greater than **26** to be able to discern whether signet is flawed of perfected.

</div>
