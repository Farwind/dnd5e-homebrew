# Ritualist

Ritualists channel other-worldly energies that summon allies from the void and employ mystic binding rituals that bend those allies to the Ritualist's will. They commune with spirits that grant great power and protection to Ritualists and their comrades. The energy they channel drives Ritualist skills which enhance the deadliness of an ally's weapon and wreak havoc on an enemy's health. The Ritualist can also use the remains of the dead to defend the living -- not by reanimating corpses as a Necromancer would, but through the ritual use of urns and ashes. Where some live as one with the spirit world, the Ritualist can and will be its master.

### Ancient Traditions

The Ritualists are one of the oldest professions, existing long before mortals had access to magic. Before magic, the Ritualists focused on channeling spirits, relying upon the strength and wisdom granted to them by their powerful ancestors whom maintained a connection to their descendants. Through their spirits, the Ritualists were able to practice magic, or something close to it. When magic was discovered, many of the original abilities were strengthened and merged into their modern form.

### TODO: Motivations

### Creating a Ritualist

TODO: General Roleplay Advice

#### Quick Build

TODO: Quick Build

#### Optional Rule: Multiclassing

If your group uses the optional rule on multiclassing in the *Player's Handbook*, here's what you need to know if you choose ritualist as one of your classes.

***Ability Score Minimum.*** As a multiclass character, you must have at least a Wisdom score of 13 to take a level in this class, or to take a level in another class if you are already a ritualist.

***Proficiencies Gained.*** If ritualist isn't your initial class, here are the proficiencies you gain when you take your first level as a ritualist: light armor, calligraphy tools.

***Spell Slots.*** Add half your levels (rounded up) in the ritualist class to the appropriate levels from other classes to determine your available spell slots.

\pagebreak

<div class='classTable wide'>

##### Ritualist
| Level | Proficiency Bonus | Features | Binding Ritual<br/> Level | Binding Ritual<br/> Slots |
|:---:|:---:|:---|:---:|:---:|
| 1st | +2 | Ethereal Perception | — | — |
| 2nd | +2 | ─ | — | — |
| 3rd | +2 | Ancient Tradition | — | — |
| 4th | +2 | Ability Score Improvement | — | — |
| 5th | +3 | Ancient Tradition feature | — | — |
| 6th | +3 | ─ | — | — |
| 7th | +3 | ─ | — | — |
| 8th | +3 | Ability Score Improvement | — | — |
| 9th | +3 | Ancient Tradition feature | — | — |
| 10th | +4 | ─ | — | — |
| 11th | +4 | ─ | — | — |
| 12th | +4 | Ability Score Improvement | — | — |
| 13th | +4 | ─ | — | — |
| 14th | +4 | ─ | — | — |
| 15th | +5 | Ancient Tradition feature | — | — |
| 16th | +5 | Ability Score Improvement | — | — |
| 17th | +5 | ─ | — | — |
| 18th | +6 | ─ | — | — |
| 19th | +6 | Ability Score Improvement | — | — |
| 20th | +6 | ─ | ─ | — |

</div>

## Class Features

As a ritualist, you gain the following class features

#### Hit Points
___
- **Hit Dice:** 1d6 per ritualist level
- **Hit Points at 1st Level:** 6 + your Constitution modifier
- **Hit Points at Higher Levels:** 1d6 (or 4) + your Constitution modifier per ritualist level after 1st

#### Proficiencies
___
- **Armor:** None
- **Weapons:** Daggers, darts, slings, quarterstaffs
- **Tools:** Calligrapher's supplies 
___
- **Saving Throws:** Charisma, Wisdom
- **Skills:** Choose two from Arcana, History, Insight, Investigation, Medicine, Perception, and Religion

#### Equipment
You start with the following equipment, in addition to the equipment granted by your background:
- *(a)* a quarterstaff or *(b)* a dagger
- *(a)* a priest's pack or *(b)* a scholar's pack
- calligrapher's supplies and a arcane focus

If you forgo this starting equipment, as well as the items offered by your background, you start with 5d4 x 10 gp to buy your equipment.

#### Ethereal Perception

At 1st level, you learn the ability to extend your senses to perceive the ethereal realm. As an Bonus Action, you may close your eyes and focus, allowing you to obtain a vague sense of your current surroundings in the ethereal plane.

While you are focused, you gain the following benefits within a 15 foot sphere centered on your position:

* You gain advantage on Wisdom (Perception) checks to detect the presence and position of nearby non-construct creatures.
* You gain a vague familiarity with the topology in the nearby ethereal plane.
* You can sense the presence and position of nearby spirits in both the material and ethereal planes.

#### Ancient Tradition

At 3rd level, you choose an ancient tradition from the list of ancient traditions, shaping your connection to the ethereal plane. Your choice grants you features at 3rd level and again at 5th, 9th, and 15th level.

#### Ability Score Improvement

When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can't increase an ability score above 20 using this feature.
